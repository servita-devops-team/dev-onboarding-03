## Servita Onboarding 03

### 1. Setup project
1. Install the requirements using `pip3 install -r requirements.txt`
2. The file `main.py` contains a basic Flask app. This app is not functional because no routes have been defined. Test that the app can run without error by executing the command `python3 main.py` at the command line.

### 2. Configure Flask app

1. Set the app to listen on port 8000.
3. Create a GET route called `/aggregate` to return `Hello World` and test this works by opening `localhost:8000/aggregate` in the browser.
5. In the function for the GET route, implement the below.

### 3. Get data to work with
1. Get the data from `GET https://directory.spineservices.nhs.uk/STU3/Organization/N81082` and test it worked by returning the data to the browser.
2. The data returned is a JSON string. Parse this string into a Python dict. Test that it worked by returning this dict directly to the browser (the browser should now format it correctly).  
### 4. Build "valueCoding" dictionary

1. Look through the data dict and find the objects called `valueCoding`, like this:
```json
"valueCoding":{
    "system": "https://directory.spineservices.nhs.uk/STU3/CodeSystem/ODSAPI-OrganizationRole-1",
    "code": "177",
    "display": "PRESCRIBING COST CENTRE"
}
```
2. Create a dictionary called `valueCodings` in which the keys are the `code` and the values are the `display` from these objects. So once your code has processed the object in Step 3, your `valueCodings` dict will look like this:

```python
{
    "177": "PRESCRIBING COST CENTRE"
}    
```

3. NB: This is an example, not the final output. Your code needs to populate `valueCodings` with every instance found in the data. Once complete, return this dict to the browser.

### 5. Find max and min "start" datetimes

1. Look through the data dict and find the objects called `valuePeriod` like this:

```json
"valuePeriod":
{
    "extension":
    [
        {
            "url": "https://fhir.nhs.uk/STU3/StructureDefinition/Extension-ODSAPI-DateType-1",
            "valueString": "Operational"
        }
    ],
    "start": "1974-04-01"
}
```

2. Modify your existing code to build a dict called `results` that contains the maximum and minimum `start` dates. There is a function called `to_date()` in `main.py` which will convert these date strings to datetimes for you. 

3. Add the `valueCodings` dict from the previous section to the `results` dict so the final dict has this structure:

```python
{
    'minStart': datetime,
    'maxStart': datetime,
    'valueCodings': {
        "key": str
    }    
} 
```
4. Return the `results` object to the browser
