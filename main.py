from flask import Flask
from flask import request

from datetime import datetime as dt

app = Flask(__name__)

def to_date(string_date):
    return dt.strptime(string_date, '%Y-%m-%d')

# Start coding here

if __name__ == '__main__':
    app.run(host="localhost", debug=True)